<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h1 align="center">Laravel Forum</h1>  
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>   
    <li><a href="#contributing">Contributing</a></li>    
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

A forum where authenticated users can post comments and start new discussions while unauthenticated users can only view the existing discussions and comments. The admin user has the ability to approve,decline,edit and delete discussions such as edit and delete unwanted comments.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

-   [Laravel](https://laravel.com/docs/8.x/)
-   [Tailwind](https://tailwindcss.com/)
-   [JQuery](https://jquery.com)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

This is how you can set up this project locally. To get a local copy up and running follow these simple example steps.

### Prerequisites

In order to be able to run this project you need to have installed

-   php
    ```sh
    https://www.php.net/downloads
    ```
-   apache
    ```sh
    https://www.apachelounge.com/download
    ```
-   mysql
    ```sh
    https://dev.mysql.com/downloads/installer
    ```
-   Composer
    ```sh
    https://getcomposer.org/
    ```
-   NPM
    ```sh
    https://nodejs.org/en/
    ```

### Installation

1. Clone the repo
    ```sh
    git clone https://gitlab.com/jordan.janevski11/laravel-forum
    ```
2. Install all the packages of new application
    ```sh
    composer install
    ```
3. Create the env file
    ```sh
    cp .env.example .env
    ```
4. Generate a key
    ```
    php artisan key:generate
    ```
5. Modify the env file with your database credentials
    ```
    DB_DATABASE = example_db_name
    DB_USERNAME = example_user
    DB_PASSWORD = example_password
    ```
6. Create the tables in the database

    ```
    php artisan migrate
    ```

7. Fill the tables with dummy data

    ```
    php artisan db:seed
    ```

8. Serve the project

    ```
    php artisan serve
    ```

    or if there is some other project running at port 8000

    ```
    php artisan serve --port=8001
    ```

9. Access the project at http://localhost:8000/ or the custom port that you set up

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Jordan Janevski - jordan.janevski@live.com

Project Link: [https://gitlab.com/jordan.janevski11/laravel-forum](https://gitlab.com/jordan.janevski11/laravel-forum)

<p align="right">(<a href="#top">back to top</a>)</p>
