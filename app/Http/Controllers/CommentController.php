<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Discussion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $discussion)
    {
        $existingAndApprovedDiscussion = Discussion::where('id', $discussion)->where('is_approved', 1)->first();

        if ($existingAndApprovedDiscussion) return redirect()->route('discussions.show', ['discussion' => $existingAndApprovedDiscussion]);

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $discussion = Discussion::where('id', $request->discussion_id)->where('is_approved', 1)->first();

        if (!$discussion) return response()->json(['status' => 400, 'message' => 'Error',]);

        $comment = new Comment();

        $comment->text = $request->text;
        $comment->discussion_id = $request->discussion_id;
        $comment->user_id = Auth::id();

        if (!$comment->save()) return response()->json(['status' => 400, 'message' => 'Error',]);

        $view = view('layouts.comment', compact('comment'))->with('success', 'Commented successfully!')->render();

        return response()->json(['status' => 200, 'view' => $view,]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        if (Gate::allows('authorOrAdmin', $comment)) return $comment;

        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        if (Gate::allows('authorOrAdmin', $comment)) {
            $comment->text = $request->text;

            if (!$comment->save()) return response()->json(['status' => 400, 'message' => 'Error',]);

            return response()->json(['status' => 200, compact('comment')]);
        }

        return response()->json(['status' => 400, 'message' => 'Error',]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if (Gate::allows('authorOrAdmin', $comment)) {
            if (!$comment->delete()) return response()->json(['status' => 400, 'message' => 'Error',]);

            return response()->json(['status' => 200, 'message' => 'Success',]);
        }

        return response()->json(['status' => 400, 'message' => 'Error',]);
    }
}
