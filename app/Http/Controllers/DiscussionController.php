<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Discussion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests\DiscussionRequest;

class DiscussionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
        $this->middleware('adminApprove')->only('edit', 'update', 'destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discussions = Discussion::where('is_approved', 1)->with('category', 'user')->paginate(5);
        return view('home', compact('discussions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('discussions.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscussionRequest $request)
    {

        $discussion = new Discussion();
        $discussion->title = $request->title;
        $discussion->description = $request->description;
        $discussion->category_id = $request->category_id;
        $discussion->user_id = Auth::id();

        if (Auth::user()->isAdmin()) $discussion->is_approved = 1;

        if ($request->image) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images/discussions'), $imageName);
            $discussion->image = "/images/discussions/{$imageName}";
        }

        if (!$discussion->save()) return redirect()->route('discussions.create')->with('error', 'Discussion could not be created!');

        $message = 'Discussion successfuly created! It needs to be approved before you dig into it though!';

        if (Auth::user()->isAdmin()) $message = 'Discussion successfuly created! Its approved since you are an admin!';

        return redirect()->route('home')->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Discussion $discussion)
    {
        if ((Auth::user() && Auth::user()->isAdmin()) || $discussion->is_approved) return view('discussions.show', compact('discussion'));

        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Discussion $discussion)
    {
        $categories = Category::all();
        return view('discussions.edit', compact('discussion', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DiscussionRequest $request, Discussion $discussion)
    {

        if ($request->image) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images/discussions'), $imageName);
            $discussion->image = "/images/discussions/{$imageName}";
            $imagePath = "/images/discussions/{$imageName}";
            $oldImagePath = public_path($request->currentImage);

            if (File::exists($oldImagePath))  File::delete($oldImagePath);
        } else {
            $imagePath = $request->currentImage;
        }

        $discussion->title = $request->title;
        $discussion->description = $request->description;
        $discussion->category_id = $request->category_id;
        ($request->is_approved) ? $discussion->is_approved = 1 : $discussion->is_approved = 0;
        $discussion->image = $imagePath;

        if (!$discussion->save()) return redirect()->route('discussions.edit', $discussion->id)->with('error', 'Discussion could not be edited!');

        return redirect()->route('discussions.edit', $discussion->id)->with('success', 'Discussion edited successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discussion $discussion)
    {
        $image = public_path($discussion->image);

        if (!$discussion->delete())  return response()->json(['status' => 400, 'message' => 'Error',]);

        if (File::exists($image)) File::delete($image);

        return response()->json(['status' => 200, 'message' => 'Success',]);
    }

    //rendering via AJAX

    public function notApproved()
    {
        $discussions = Discussion::where('is_approved', 0)->with('category', 'user')->paginate(5);
        return view('discussions.approve', compact('discussions'));
    }

    public function renderNotApproved()
    {
        $discussions = Discussion::where('is_approved', 0)->with('category', 'user')->paginate(5);
        $approved = false;
        return view('layouts.discussions', compact('discussions', 'approved'))->render();
    }

    public function renderApproved()
    {
        $discussions = Discussion::where('is_approved', 1)->with('category', 'user')->paginate(5);
        $approved = true;
        return view('layouts.discussions', compact('discussions', 'approved'))->render();
    }

    // updating via AJAX

    public function approve(Request $request, Discussion $discussion)
    {
        $discussion->is_approved = 1;

        if (!$discussion->save()) return response()->json(['status' => 400, 'message' => 'Error',]);

        return response()->json(['status' => 200, 'message' => 'Success',]);
    }
}
