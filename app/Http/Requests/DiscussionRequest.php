<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class DiscussionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Request::route()->getName();

        $rules = [
            'title' => ['required'],
            'description' => ['required'],
            'category_id' => ['required'],
        ];

        $rules['image'] = $route == 'discussions.store' ? ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:5120'] : '';

        return $rules;
    }
}
