<?php

namespace Database\Seeders;

use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Comment::create(
                [
                    'text' => $faker->text,
                    'discussion_id' => rand(1, 2),
                    'user_id' => rand(1, 3)
                ]
            );
        }
    }
}
