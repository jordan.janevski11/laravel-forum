<?php

namespace Database\Seeders;

use App\Models\Discussion;
use Illuminate\Database\Seeder;

class DiscussionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        Discussion::create(
            [
                'title' => 'Discussion 1',
                'description' => $faker->text,
                'category_id' => rand(1, 6),
                'image' => '/images/discussions/discussion1.jpg',
                'is_approved' => 1,
                'user_id' => rand(1, 3)
            ]
        );

        Discussion::create(
            [
                'title' => 'Discussion 2',
                'description' => $faker->text,
                'category_id' => rand(1, 6),
                'image' => '/images/discussions/discussion2.jpg',
                'is_approved' => 1,
                'user_id' => rand(1, 3)
            ]
        );
    }
}
