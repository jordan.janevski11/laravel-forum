<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->username = 'Admin';
        $admin->email = 'admin@example.com';
        $admin->password = Hash::make('123123123');
        $admin->role = 'admin';
        $admin->save();

        $user1 = new User();
        $user1->username = 'User1';
        $user1->email = 'user1@example.com';
        $user1->password = Hash::make('123123123');
        $user1->save();

        $user2 = new User();
        $user2->username = 'User2';
        $user2->email = 'user2@example.com';
        $user2->password = Hash::make('123123123');
        $user2->save();
    }
}
