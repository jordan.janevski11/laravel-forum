$(function () {
    $(document).on("click", "#show_comment_create", function (e) {
        e.preventDefault();

        Swal.fire({
            input: "textarea",
            inputLabel: "Comment",
            inputPlaceholder: "Leave a comment here...",
            inputAttributes: {
                name: "commentCreate",
            },
            showCancelButton: true,
            preConfirm: () => {
                if ($('textarea[name="commentCreate"]').val().trim() == "") {
                    Swal.showValidationMessage(`Comment can't be empty`);
                }
            },
        }).then((result) => {
            if (result.isConfirmed) {
                let comment = $('textarea[name="commentCreate"]').val();
                let discussion_id = window.location.pathname.split("/")[2];
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                });

                $.ajax({
                    type: "POST",
                    url: $(this).attr("href"),
                    data: {
                        text: comment,
                        discussion_id: discussion_id,
                    },
                    dataType: "json",
                    error: function (xhr) {
                        Swal.fire(
                            "Error!",
                            "Comment could not be created.",
                            "error"
                        );
                    },
                }).then((response) => {
                    Swal.fire(
                        "Commented successfuly!",
                        "Comment has been created.",
                        "success"
                    );
                    $("#comments").append(response.view);
                    setTimeout(function () {
                        $(document).scrollTop($(document).height());
                        $("#comments .comment:last-child")
                            .fadeOut(500)
                            .fadeIn(500);
                    }, 2000);
                });
            }
        });
    });
});
