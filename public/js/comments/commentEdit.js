$(function () {
    $(document).on("click", "#show_comment_edit", function (e) {
        e.preventDefault();
        let commentId = "";
        let comment = "";

        $.get($(this).attr("href")).then((data) => {
            commentId = data.id;
            comment = data.text;

            Swal.fire({
                input: "textarea",
                inputValue: comment,
                inputLabel: "Edit Comment",
                inputAttributes: {
                    name: "commentEdit",
                },
                showCancelButton: true,
                preConfirm: () => {
                    if ($('textarea[name="commentEdit"]').val().trim() == "") {
                        Swal.showValidationMessage(`Comment can't be empty`);
                    }
                },
            }).then((result) => {
                if (result.isConfirmed) {
                    let updatedComment = $(
                        'textarea[name="commentEdit"]'
                    ).val();
                    $.ajaxSetup({
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                                "content"
                            ),
                        },
                    });

                    $.ajax({
                        type: "POST",
                        url: `${window.location.origin}/comments/${commentId}`,
                        data: {
                            text: updatedComment,
                            _method: "PUT",
                        },
                        dataType: "json",
                        error: function (xhr) {
                            Swal.fire(
                                "Error!",
                                "Comment could not be edited.",
                                "error"
                            );
                        },
                    }).then((response) => {
                        Swal.fire(
                            "Comment edited successfuly!",
                            "Comment has been edited.",
                            "success"
                        );

                        $(this)
                            .parents(".comment")
                            .find("p")
                            .text(response[0].comment.text);
                        $(this)
                            .parents(".comment")
                            .find(".updatedAt")
                            .text(
                                response[0].comment.updated_at
                                    .replace(/T/, " ")
                                    .replace(/\..+/, "")
                            );

                        setTimeout(function () {
                            // $(document).scrollTop($(document).height());
                            $(this).parents(".comment").css({ border: "red" });
                        }, 1000);
                    });
                }
            });
        });
    });
});
