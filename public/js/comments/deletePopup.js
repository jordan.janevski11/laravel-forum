$(function () {
    $(document).on("click", ".show_confirm", function (e) {
        e.preventDefault();
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                });

                $.ajax({
                    type: "POST",
                    url: $(this).attr("href"),
                    data: { _method: "DELETE" },
                    dataType: "json",
                    error: function (xhr) {
                        Swal.fire(
                            "Error!",
                            "Comment could not be deleted.",
                            "error"
                        );
                    },
                }).then((response) => {
                    if (response.status != 200) {
                        Swal.fire(
                            "Error!",
                            "Comment could not be deleted.",
                            "error"
                        );
                        return -1;
                    }

                    $(this).parents(`.comment`).hide();
                    Swal.fire(
                        "Deleted!",
                        "Comment has been deleted.",
                        "success"
                    );
                });
            }
        });
    });
});
