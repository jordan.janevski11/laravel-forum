$(function () {
    $(document).on("click", "#approvedFilter", function (e) {
        $(e.target).addClass("border-blue-500");
        $("#notapprovedFilter").removeClass("border-red-500");
        $.get("/approved")
            .then((data) => {
                $("#discussions").html(data);
                $("#myPagination").find("p").hide();
            })
            .catch((err) => console.log(err));
    });

    $(document).on("click", "#notapprovedFilter", function (e) {
        $(e.target).addClass("border-red-500");
        $("#approvedFilter").removeClass("border-blue-500");
        $.get("/notapproved")
            .then((data) => {
                $("#discussions").html(data);
                $("#myPagination").find("p").hide();
            })
            .catch((err) => console.log(err));
    });

    $(document).on("click", "#myPagination a", function (e) {
        e.preventDefault();
        let page = $(this).attr("href").split("page=")[1];
        let url = $(this).attr("href").split("?")[0];
        let method = url.substring(url.lastIndexOf("/") + 1);
        $.get(`/${method}?page=${page}`)
            .then((data) => {
                $("#discussions").html(data);
                $("#myPagination").find("p").hide();
            })
            .catch((err) => console.log(err));
    });
});
