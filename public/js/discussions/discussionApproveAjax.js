$(function () {
    $(document).on("click", ".show_approve", function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            type: "POST",
            url: $(this).attr("href"),
            data: { _method: "PUT" },
            dataType: "json",
            error: function (xhr) {
                Swal.fire(
                    "Error!",
                    "Discussion could not be approved.",
                    "error"
                );
            },
        }).then((response) => {
            if (response.status != 200) {
                Swal.fire(
                    "Error!",
                    "Discussion could not be approved.",
                    "error"
                );
                return -1;
            }

            $(this).parents(`.discussion`).hide();
            $(this).parents(`.discussion`).addClass("modified");

            Swal.fire("Approved!", "Discussion has been approved.", "success");

            if ($(".discussion").length == $(".modified").length) {
                $.get("/notapproved")
                    .then((data) => {
                        $("#discussions").html(data);
                    })
                    .catch((err) => console.log(err));
            }
        });
    });
});
