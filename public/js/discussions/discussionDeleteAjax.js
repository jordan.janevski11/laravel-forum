$(function () {
    $(document).on("click", ".show_confirm", function (e) {
        e.preventDefault();
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                });

                $.ajax({
                    type: "POST",
                    url: $(this).attr("href"),
                    data: { _method: "DELETE" },
                    dataType: "json",
                    error: function (xhr) {
                        Swal.fire(
                            "Error!",
                            "Discussion could not be deleted.",
                            "error"
                        );
                    },
                }).then((response) => {
                    if (response.status != 200) {
                        Swal.fire(
                            "Error!",
                            "Discussion could not be deleted.",
                            "error"
                        );
                        return -1;
                    }

                    $(this).parents(`.discussion`).hide();
                    $(this).parents(`.discussion`).addClass("modified");
                    Swal.fire(
                        "Deleted!",
                        "Discussion has been deleted.",
                        "success"
                    );

                    $.get("/notapproved")
                        .then((data) => {
                            $("#discussions").html(data);
                        })
                        .catch((err) => console.log(err));

                    let route = "/notapproved";
                    if (
                        $("#typeOfDiscussions").data("type") ==
                        "approvedDiscussions"
                    ) {
                        route = "/approved";
                    }
                    if ($(".discussion").length == $(".modified").length) {
                        $.get(route)
                            .then((data) => {
                                $("#discussions").html(data);
                            })
                            .catch((err) => console.log(err));
                    }
                });
            }
        });
    });
});
