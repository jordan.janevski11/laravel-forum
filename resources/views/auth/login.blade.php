<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
        @if (session('autherror'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                <span class="block sm:inline">{{ session('autherror') }}</span>               
            </div>
        @endif      
            
        </x-slot>
            
        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <form method="POST" action="{{ route('login') }}">
            @csrf

            @php
                $errorClasses = 'border-2 border-rose-500';

                $usernameError = $errors->has('username') ? $errorClasses : '';
                $passwordError = $errors->has('password') ? $errorClasses : '';
            @endphp

            <!-- Username -->
            <div>
                <x-label for="username" :value="__('Username')" />
                <x-input class="w-full mt-1 {{ $usernameError }}" id="username" type="text" name="username" :value="old('username')"/>
            </div>

            @error('username')
            <span class="text-red-500 text-xs italic" role="alert">
                {{ $message }}
            </span>
            @enderror

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />
                <x-input class="w-full mt-1 {{ $passwordError }}" id="password" type="password" name="password" autocomplete="current-password"/>
            </div>
            @error('password')
            <span class="text-red-500 text-xs italic" role="alert">
                {{ $message }}
            </span>
            @enderror
            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                    @endif
                    <a class="ml-2 underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('register') }}">
                        {{ __('Register Here') }}
                    </a>

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
