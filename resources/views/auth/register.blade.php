<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        {{-- <x-auth-validation-errors class="mb-4" :errors="$errors" /> --}}

        <form method="POST" action="{{ route('register') }}">
            @csrf

            @php
                $errorClasses = 'border-2 border-rose-500';

                $usernameError = $errors->has('username') ? $errorClasses : '';
                $emailError = $errors->has('email') ? $errorClasses : '';
                $passwordError = $errors->has('password') ? $errorClasses : '';
                
            @endphp

            <!-- Username -->
            <div>
                <x-label for="username" :value="__('Username')" />
                <x-input id="username" class="{{ $usernameError }} block mt-1 w-full" type="text" name="username" :value="old('username')"/>
            </div>

            @error('username')
            <span class="text-red-500 text-xs italic" role="alert">
                {{ $message }}
            </span>
            @enderror

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />
                <x-input id="email" class="{{ $emailError }} block mt-1 w-full" type="email" name="email" :value="old('email')" />
            </div>

            @error('email')
            <span class="text-red-500 text-xs italic" role="alert">
                {{ $message }}
            </span>
            @enderror

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="{{ $passwordError }} block mt-1 w-full"
                                type="password"
                                name="password"
                                autocomplete="new-password" />
            </div>

            @error('password')
            <span class="text-red-500 text-xs italic" role="alert">
                {{ $message }}
            </span>
            @enderror

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="{{ $passwordError }} block mt-1 w-full"
                                type="password"
                                name="password_confirmation" />
            </div>

            @error('passwordError')
            <span class="text-red-500 text-xs italic" role="alert">
                {{ $message }}
            </span>
            @enderror

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
