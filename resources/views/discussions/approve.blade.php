<x-app-layout>
    <x-slot name="header">
        
        @include('layouts.sessionAlert')

        <h2 class="font-semibold text-4xl text-gray-800 leading-tight text-center">
            {{ __('Welcome to the forum') }}
        </h2>
    </x-slot>

    
    @include('layouts.backButton', ['route'=>'home'])

    
    
    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                
            <button class="h-10  bg-white tracking-wide text-gray-800 font-bold rounded border-b-2  hover:border-blue-600 hover:bg-blue-500 hover:text-white shadow-md py-2 px-6 inline-flex items-center" id='approvedFilter' types='button'>Approved</button>
            <button class="h-10  bg-white tracking-wide text-gray-800 font-bold rounded border-b-2 border-red-500 hover:border-red-600 hover:bg-red-500 hover:text-white shadow-md py-2 px-6 inline-flex items-center" id='notapprovedFilter' types='button'>Not Approved</button>
            
        </div>
    </div>
    
    <div id="discussions">
        @include('layouts.discussions')        
    </div>      
   
   
</x-app-layout>