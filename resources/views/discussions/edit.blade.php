<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-4xl text-gray-800 leading-tight text-center">
            {{ __('Edit discussion') }}
        </h2>
    </x-slot>

    
    @include('layouts.sessionAlert')

    @include('layouts.backButton', ['route'=>'discussions.notapproved' ])

    <div class="pb-12">
        <div class="w-full max-w-lg mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form class="w-full max-w-lg" method="POST" enctype="multipart/form-data" action="{{ route('discussions.update', $discussion->id) }}">
                        @csrf
                        @method('PUT')
                        @php
                            $errorClasses = ' border-rose-500';

                            $titleError = $errors->has('title') ? $errorClasses : '';
                            $imageError = $errors->has('image') ? $errorClasses : '';
                            $descriptionError = $errors->has('description') ? $errorClasses : '';
                            $categoryError = $errors->has('category') ? $errorClasses : '';
                        @endphp
            
                        <!-- Title -->
                        <div>
                            <x-label for="title" :value="__('Title')" />
                            <x-input class="w-full mt-1 {{ $titleError }}" id="title" type="text" name="title" value="{{ $discussion->title }}"/>
                        </div>
            
                        @error('title')
                            <span class="text-red-500 text-xs italic" role="alert">
                                {{ $message }}
                            </span>
                        @enderror

                        <input type="hidden" name="currentImage" value="{{ $discussion->image }}">

                        <div class="md:shrink-0 flex items-center justify-center pt-5">
                            <img class="h-48 w-full object-cover md:w-48" src="{{ $discussion->image }}" alt="discussionImage">   
                        </div>
            
                        <!-- Image -->

                        <div class="flex flex-wrap -mx-3 mt-4">
                            <div class="w-full px-3">
                              <label class="block tracking-wide text-gray-700 text-sm border-gray-300 font-medium" for="image">
                                New Image
                              </label>                       
                          
                              
                              <input class="mt-1 block w-full rounded-md shadow-sm py-2 px-3 border border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 {{ $imageError }}" type="file" name="image" id="image">
                            </div>
                        </div>
   
                        @error('image')
                            <span class="text-red-500 text-xs italic" role="alert">
                                {{ $message }}
                            </span>
                        @enderror


                        <!-- Approved -->

                        <div class="flex justify-center mt-4">
                            <label class="inline-flex items-center">
                                <input type="checkbox" name="is_approved" class="form-checkbox" @if($discussion->is_approved == 1) checked  @endif>
                                <span class="ml-2">Approved</span>
                            </label>
                        </div>
                        

                        <!-- Description -->

                        <div class="mt-4">
                        <x-label for="description" :value="__('Description')" />
                        <textarea name="description" id="description" class="mt-1 block w-full rounded-md shadow-sm py-2 px-3 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 {{ $descriptionError }}">{{ $discussion->description }} </textarea>
                        </div>

                        @error('description')
                            <span class="text-red-500 text-xs italic" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                        <!-- Category -->

                        <div class="mt-4">
                            <x-label for="category" :value="__('Category')" />
                            <select id="categorySelect" class="mt-1 block w-full rounded-md shadow-sm py-2 px-3 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 {{ $categoryError }}" name="category_id" id="category_id">
                                @isset($categories)
                                    @foreach ($categories as $category )
                                        <option value="{{$category->id}}" @if($category->id == $discussion->category_id) selected @endif>{{$category->name}}</option>
                                    @endforeach                                
                                @endisset            
                            </select>
                        </div>
                        @error('category_id')
                            <span class="text-red-500 text-xs italic" role="alert">
                                {{ $message }}
                            </span>
                        @enderror

                        <!--Submit-->
                        <div class="flex items-center mt-4">                                      
                            <x-button class="bg-blue-400 hover:bg-blue-700">
                                {{ __('Update') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

   
</x-app-layout>