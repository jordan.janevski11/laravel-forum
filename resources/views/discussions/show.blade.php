<x-app-layout>
    <x-slot name="header">

        @include('layouts.sessionAlert')
        
        <h2 class="font-semibold text-4xl text-gray-800 leading-tight text-center">
            {{ __('Welcome to the forum') }}
        </h2>
    </x-slot>
    
    @isset($discussion)       
        <div class="py-3">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200">
                        <div class="ml-auto flex items- justify-between text-sm text-gray-500">      
                            <div>
                                @include('layouts.backButton', ['route'=>'discussions.index'])
                            </div>                       
                            <span>{{ $discussion->category->name }} | {{ $discussion->user->username }}</span>
                        </div>

                        <div class="md:shrink-0 flex items-center justify-center p-12 flex-col">
                                <img class="w-3/4 object-cover" src="{{ $discussion->image }}" alt="discussionImage">
                        </div>
                        
                        <div class="lg:px-48"> 
                            <h4 class="text-3xl font-semibold">
                                {{ $discussion->title }}
                            </h4>
                            <p> {{ $discussion->description }} </p>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>     
    @endisset

    
    <div class="py-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h4 class="text-3xl font-semibold">
                Comments :
            </h4>
        </div>
    </div>

    <div class="py-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(Auth::check())
            <a id="show_comment_create" class="py-2 px-6 font-semibold rounded-md bg-gray-500 hover:bg-gray-700 text-white" href="{{ route('comments.store') }}">Comment</a>
            @else
            <a class="py-2 px-6 font-semibold rounded-md bg-gray-500 hover:bg-gray-700 text-white" href="{{ route('comments.createCustom', $discussion->id)}}">Comment</a>
            @endauth
            
        </div>
    </div>

    {{-- @if (!$discussion->comments->isEmpty())         --}}
    <div id="comments">
        @foreach ($discussion->comments as $comment)
            @include('layouts.comment', $comment)        
        @endforeach
    </div>
    {{-- @endif --}}
   
</x-app-layout>