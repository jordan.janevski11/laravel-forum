<x-app-layout>
    <x-slot name="header">
        @if(Session::has('success'))
            <div class="flex items-center justify-center">
                <div class="bg-green-100 border border-green-400 text-green-700 w-2/3 text-center px-4 py-3 mb-5 rounded" role="alert">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
        <h2 class="font-semibold text-4xl text-gray-800 leading-tight text-center">
            {{ __('Welcome to the forum') }}
        </h2>
    </x-slot>

    <div class="py-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <a href="{{ route('discussions.create') }}">
                <button class="h-10 px-6 font-semibold rounded-md bg-gray-500 hover:bg-gray-700 text-white">
                    Add new discussion
                </button>
            </a>
        </div>
    </div>
    @auth   
        @if (Auth::user()->isAdmin())
            <div class="py-5">
                <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <a href="{{ route('discussions.notapproved') }}">
                        <button class="h-10 px-6 font-semibold rounded-md bg-blue-500 hover:bg-blue-700 text-white">
                            Approve discussions
                        </button>
                    </a>
                </div>
            </div>
        @endif
    @endauth
    @isset($discussions)
        @if ($discussions->isEmpty())
        <h2 class="font-semibold text-xl text-gray-500 leading-tight text-center">
           Nothing here yet! Start a topic!
        </h2>

        @else

        @foreach ($discussions as $discussion)
        <div class="py-3">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <a href="{{ route('discussions.show', $discussion->id) }}">
                    <div class="p-6 bg-white hover:bg-gray-200 border-b border-gray-200 flex">
                        <div class="md:shrink-0">
                            <img class="h-48 w-full object-cover md:w-48" src="{{ $discussion->image }}" alt="discussionImage">
                        </div>
                        <div class="ml-3 flex justify-center flex-col"> 
                            <h4 class="font-semibold">
                                {{ $discussion->title }}
                            </h4>
                            <p> {{ $discussion->description }} </p>
                        </div>
                        <div class="ml-auto flex items-center text-sm text-gray-500">                             
                            <span>{{ $discussion->category->name }} | {{ $discussion->user->username }}</span>
                        </div>
                    </div>
                </a>
                </div>
            </div>
        </div>
        @endforeach
       

        @endif            
        
        <div class="p-3">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="p-3 overflow-hidden  sm:rounded-lg">
                    {{$discussions->links()}}
                </div>
            </div>
        </div>
    @endisset


   

</x-app-layout>