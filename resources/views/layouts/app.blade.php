<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->

        {{-- jQuery --}}
        <script
        src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>

        {{-- sweetalert cdn --}}
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        {{-- delete popup for comments --}}
        @if (Route::is('discussions.show'))
        <script src="{{ asset('js/comments/deletePopup.js') }}" defer></script>
        <script src="{{ asset('js/comments/commentCreate.js') }}" defer></script>
        <script src="{{ asset('js/comments/commentEdit.js') }}" defer></script>
        @endif

        {{-- AJAX for approved --}}
        @if (Route::is('discussions.notapproved'))
        <script src="{{ asset('js/discussions/approveAjax.js') }}" defer></script>            
        <script src="{{ asset('js/discussions/discussionDeleteAjax.js') }}" defer></script>            
        <script src="{{ asset('js/discussions/discussionApproveAjax.js') }}" defer></script>            
        @endif


        <script src="{{ asset('js/app.js') }}" defer></script>


    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </body>
</html>
