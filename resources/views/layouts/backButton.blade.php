<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @php
            if (!isset($param))
                $param = '';
        @endphp
        <a class="py-2 px-6 font-semibold rounded-md bg-gray-500 hover:bg-gray-700 text-white" href="{{ route($route, $param) }}">
                Go back            
        </a>
    </div>
</div>