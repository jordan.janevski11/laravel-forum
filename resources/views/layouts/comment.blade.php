<div class="py-3 comment">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
           
            <div class="p-6 bg-white border-b border-gray-200">
                
                <div class="flex items-center justify-between text-sm text-gray-500">                            
                    <span class="font-semibold">{{ $comment->user->username }} says:</span>     
                        
                    <span class="updatedAt">{{ $comment->updated_at }}</span>
                </div>

                <div class="ml-3 mt-3 flex justify-center flex-col">                             
                    <p> {{$comment->text }}</p>
                    <div class="ml-auto flex items-center text-sm text-gray-500"> 
                        @auth
                            @if (Auth::id() == $comment->user_id || Auth::user()->isAdmin())                                   
                            <div class="flex item-center justify-center">
                                
                                <a id="show_comment_edit" href="{{route('comments.edit', $comment->id)}}">
                                    <div class="w-5 mr-2 transform hover:text-purple-500 hover:scale-110">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                        </svg>
                                    </div>
                                </a>                                 
                                <a href="{{route('comments.destroy', $comment->id)}}" class="show_confirm" data-toggle="tooltip" title='Delete'>
                                    <div class="w-5 mr-2 transform hover:text-purple-500 hover:scale-110">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                        </svg>
                                    </div>
                                </a>
                            </div>
                            @endif
                        @endauth
                        
                    </div>
                </div>
               
            </div>
        
        </div>
    </div>
</div>