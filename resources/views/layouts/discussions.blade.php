<span id="typeOfDiscussions"
data-type='@php
if(isset($approved)){
    if($approved){
        echo 'approvedDiscussions';
    }
}    else{
        echo 'notApprovedDiscussions';
    }
@endphp'>
</span>
@if ($discussions->isEmpty())
<h2 class="font-semibold text-xl text-gray-500 leading-tight text-center">
    Nothing here yet!
 </h2>    
@endif
@foreach ($discussions as $discussion)
<div class="py-3 discussion">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200 flex">
                <div class="md:shrink-0">
                    <img class="h-48 object-cover md:w-48" src="{{ $discussion->image }}" alt="discussionImage">
                </div>
                <div class="ml-3 flex justify-center flex-col"> 
                    <h4 class="font-semibold">
                        {{ $discussion->title }}
                    </h4>
                    <p> {{ $discussion->description }} </p>
                </div>
                <div class="ml-auto flex items-center text-sm text-gray-500"> 

                    @auth
                        @if (Auth::user()->isAdmin())
                                                          
                        <div class="flex item-center justify-center">

                        @if(!$discussion->is_approved)                                                               
                            <a href="{{route('approve.store', $discussion->id)}}" class="show_approve" data-toggle="tooltip" title='Approve'>
                                <div class="w-5 mr-2 transform hover:text-purple-500 hover:scale-110">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                                    </svg>
                                </div>
                            </a>
                        @endif                           
                            
                            <a href="{{route('discussions.edit', $discussion->id)}}">
                                <div class="w-5 mr-2 transform hover:text-purple-500 hover:scale-110">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                    </svg>
                                </div>
                            </a>                                    
                            <a href="{{route('discussions.destroy', $discussion->id)}}" class="show_confirm" data-toggle="tooltip" title='Delete'>
                                <div class="w-5 mr-2 transform hover:text-purple-500 hover:scale-110">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                    </svg>
                                </div>
                            </a>
                        </div>
                        @endif
                    @endauth
                    <span>{{ $discussion->category->name }} | {{ $discussion->user->username }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div class="p-3">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div id="myPagination" class="p-3 overflow-hidden  sm:rounded-lg">
            {{ $discussions->render() }}
        </div>
    </div>
</div>