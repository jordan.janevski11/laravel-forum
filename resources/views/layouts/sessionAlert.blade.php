@if(Session::has('error'))
    <div class="flex items-center justify-center">
        <div class="bg-red-100 border border-red-400 text-red-700 w-1/2 text-center px-4 py-3 mb-5 rounded" role="alert">
            {{ Session::get('error') }}
        </div>
    </div>
    @endif

    @if(Session::has('success'))
    <div class="flex items-center justify-center">
        <div class="bg-green-100 border border-green-400 text-green-700 w-1/2 text-center px-4 py-3 mb-5 rounded" role="alert">
            {{ Session::get('success') }}
        </div>
    </div>
@endif