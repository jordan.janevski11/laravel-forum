<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DiscussionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DiscussionController::class, 'index'])->name('home');


//============================Routes for discussions middleware in controller constructor
Route::resource('discussions', DiscussionController::class);

//============================Routes for approving
Route::middleware('adminApprove')->group(function () {
    Route::get('/approve', [DiscussionController::class, 'notApproved'])->name('discussions.notapproved');
    Route::get('/notapproved', [DiscussionController::class, 'renderNotApproved']);
    Route::get('/approved', [DiscussionController::class, 'renderApproved'])->name('discussions.approve');


    Route::put('/approve/{discussion}', [DiscussionController::class, 'approve'])->name('approve.store');
});

//============================Routes for comments 
Route::resource('comments', CommentController::class)->middleware('auth');
Route::get('/comments/create/{discussion}', [CommentController::class, 'create'])->name('comments.createCustom')->middleware('auth');

require __DIR__ . '/auth.php';
